const MODAL = "modal/MODAL"

export const toggleModal = (id) => ({ type: MODAL, payload: id })

const initialState = {
  id: null,
  isOpen: false
}

const modalReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case MODAL:
      const newState = {...state}
      newState.isOpen=!newState.isOpen
      newState.id=payload
      return newState;

    default:
      return state;
  }
}

export default modalReducer;