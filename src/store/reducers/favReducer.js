const TOGGLE_PRODUCTS = "products/TOGGLE_PRODUCTS"
const GET_FAV = "products/GET_FAV"

const toggleProducts = (id) => ({ type: TOGGLE_PRODUCTS, payload: id })

const getFav=(data)=>({type: GET_FAV, payload: data})

export const addRemoveFromFav = (id) => (dispatch) => {
    const fav = JSON.parse(localStorage.getItem('fav'))
    if(fav.includes(id)){
        const filtered= fav.filter(item=>item!==id)
        localStorage.setItem('fav', JSON.stringify(filtered))
    }
    if(!fav.includes(id)){
        const updated =  [...fav, id]
        localStorage.setItem('fav', JSON.stringify(updated))
    }
    dispatch(toggleProducts(id));
}

export const getFavFromStorage = (id) => (dispatch) => {
    const fav = JSON.parse(localStorage.getItem('fav'))
    if (!fav) {
        localStorage.setItem("fav", JSON.stringify([]))
    }
    dispatch(getFav(fav||[]));
}

const initialState = {
    fav:[]
}

const favReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case TOGGLE_PRODUCTS:
            if(state.fav.includes(payload)){
                const filtered = state.fav.filter(item=>item!==payload)
                state.fav=filtered
                return state
            }
            if(!state.fav.includes(payload)){
                const newState = {...state}
                newState.fav=[...state.fav,payload]
                return newState
            }
            break
            case GET_FAV:
                return {...state, fav: payload}
        default:
            return state;
    }
}

export default favReducer;