import { combineReducers } from 'redux';
import productsReducer from './productsReducer';
import favReducer from './favReducer';
import cartReducer from './cartReducer';
import modalReducer from './modalReducer';

const rootReducer = combineReducers({
  products: productsReducer,
  fav: favReducer,
  cart: cartReducer,
  modal: modalReducer
});

export default rootReducer