import React from 'react'
import {StyledBtn} from "./styled"
const Button = (props) => {
  const {
    text, onClick, styles,
} = props
  return (
    <StyledBtn onClick={onClick} $bgStyle = {styles} >{text}</StyledBtn>
  )
}

export default Button