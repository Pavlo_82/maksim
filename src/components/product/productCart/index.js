import React from 'react'
import { StyledImg, StyledCard, StyledButton } from "./styled"
import { AiFillHeart, AiOutlineHeart, } from 'react-icons/ai';
import { RxCross2 } from 'react-icons/rx';
import Button from "../../button";
import { useDispatch, useSelector } from 'react-redux';
import { addRemoveFromFav } from '../../../store/reducers/favReducer';
import { addToFav } from '../../../store/reducers/productsReducer'
import { toggleModal } from '../../../store/reducers/modalReducer';
import { deleteProductFromCart, setCurrent } from '../../../store/reducers/cartReducer';

const ProductCart = ({ productId, cross, button }) => {
  const product = useSelector(state => state.products[productId]) || {}
  const dispatch = useDispatch()
  const {
    title,
    imgSrc,
    price,
    article,
    color,
    fav,
    count
  } = product

  const handleClick = (id) => {
    dispatch(addRemoveFromFav(id))
    dispatch(addToFav(id))
  }

  return (

    <StyledCard>
      {cross && <RxCross2 onClick={() => {
        dispatch(toggleModal("modalDelete"))
        dispatch(setCurrent(productId))
        }} />}
      <p>{fav ? <AiFillHeart onClick={() => handleClick(productId)} /> : <AiOutlineHeart onClick={() => handleClick(productId)} />}</p>
        {cross&&<p>{count}</p>}
      <h3>count in cart: {title}</h3>
      <StyledImg src={imgSrc} />
      <p>{price}</p>
      <p>{article}</p>
      <p>{color}</p>
      {button && <Button text="Add to cart" styles="blue" onClick={() => {
        dispatch(toggleModal("modalAddToCart"))
        dispatch(setCurrent(productId))
      }} />}
    </StyledCard>
  )
}

export default ProductCart