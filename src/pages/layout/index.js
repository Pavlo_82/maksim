import React, { useEffect, useState } from 'react'
import { Routes, Route } from 'react-router-dom'
import Header from '../../components/header'
import HomePage from '../homePage'
import FavPage from '../favPage'
import CartPage from '../cartPage'
import Modal from '../../components/modal'
import { useSelector } from 'react-redux'
const LayOut = () => {
  const modal = useSelector(state=>state.modal.isOpen)
  
  return (
    <div>
      <Header/>
      <Routes>
        <Route path="/" element={<HomePage  />} />
        <Route path="/favorite" element={<FavPage />} />
        <Route path="/cart" element={<CartPage />} />
      </Routes>
      {modal&&<Modal />}
    </div>

  )
}

export default LayOut