import React from 'react'
import {ProductList} from '../../components/product/productList'
const HomePage = () => {
  
  return (
    <ProductList productListId="all"/>
  )
}

export default HomePage